// Small sweep program to test beepers

const int sPin = 8;
const unsigned long dur_ms = 100;

typedef enum { SWEEP, RESO, MELLOW } ExeType;
ExeType type = SWEEP;

// Piano's lowest  note (A0) at 27.50Hz   - index 9
// Piano's highest note (C8) at 4186,01Hz - index 96
const unsigned int c8_to_b8[] = {4186, 4435, 4699, 4978, 5274, 5588, 5920, 6272, 6645, 7040, 7459, 7902};
unsigned int mkNote(byte index) {
  if (index > 107) index = 107; // B8
  return c8_to_b8[index % 12] >> (8 - (byte)(index / 12));
}

void setup() {
  pinMode(sPin, OUTPUT);
  digitalWrite(sPin, HIGH); // if active=LOW
}

void loop() {
  noTone(sPin);
  delay(500); // wait half a second

  switch (type) {
    case SWEEP: {
        for (byte i = 24; i < 108; i++) {
          tone(sPin, mkNote(i));
          delay(dur_ms);
        }
        noTone(sPin);
        type = RESO;
      }  break;
    case RESO: {
        for (byte i = 0; i < 4; i++) {
          tone(sPin, 2731); // resonance frequency of ABT-408-RC
          delay(500);
          tone(sPin, 3000); // resonance frequency of ABT-460-RC
          delay(500);
        }
        noTone(sPin);
        type = MELLOW;
      } break;
    case MELLOW: {
        for (byte i = 0; i < 4; i++) {
          tone(sPin, 2731 >> 1);
          delay(500);
          noTone(sPin);
          delay(500);
        }
        type = SWEEP;
      } break;
  } // switch

  noTone(sPin);
}
