// Small interval output program to test e.g. beepers
// We try to simulate an OC setup

const int intervalPin = 8;


void setup() {
  pinMode(intervalPin, OUTPUT);
  digitalWrite(intervalPin, HIGH);
}

void loop() {
  delay(500); // wait half a second
  digitalWrite(intervalPin, LOW);
  delay(500); // wait half a second
  digitalWrite(intervalPin, HIGH);
}
