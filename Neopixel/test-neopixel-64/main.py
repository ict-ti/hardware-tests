# NeoPixel 8x8 Hardware Test
from machine import Pin
from time import sleep_ms
from neopixel import NeoPixel

NUM_PIX = 64
PIN_PIX = 6

pic_a = [(2,2,0),(8,8,0),(8,8,0),(8,8,0),(8,8,0),(8,8,0),(8,8,0),(2,2,0),
         (8,8,0),(0,0,2),(0,0,0),(4,4,4),(4,4,4),(0,0,0),(1,1,1),(8,8,0),
         (8,8,0),(1,0,2),(0,0,0),(4,4,4),(4,4,4),(0,0,0),(2,2,2),(8,8,0),
         (8,8,0),(0,1,2),(0,0,0),(4,4,4),(4,4,4),(0,0,0),(3,3,3),(8,8,0),
         (8,8,0),(0,0,2),(0,0,0),(4,4,4),(4,4,4),(0,0,0),(4,4,4),(8,8,0),
         (8,8,0),(0,1,0),(0,0,0),(0,0,0),(0,0,0),(0,0,0),(5,5,5),(8,8,0),
         (8,8,0),(1,0,0),(0,0,0),(4,4,4),(4,4,4),(0,0,0),(6,6,6),(8,8,0),
         (2,2,0),(8,8,0),(8,8,0),(8,8,0),(8,8,0),(8,8,0),(8,8,0),(2,2,0)]

# BIST (Built-In Self Test)
led = Pin(25, Pin.OUT)
for i in range(3):
    led.on()
    sleep_ms(200)
    led.off()
    sleep_ms(200)

# NeopPixel Test
np_pin = Pin(PIN_PIX, Pin.OUT)
np = NeoPixel(np_pin, NUM_PIX)

kleuren = [(16,0,0),(0,16,0),(0,0,16),(32,0,0),(0,32,0),(0,0,32),
           (20,20,0),(24,24,4),(28,28,8),
           (32,0,0),(16,0,0),(8,0,0),(4,0,0),(2,0,0),(1,0,0)]

while True:
    # show colour patterns
    for k in kleuren:
        for i in range(len(np)):
            np[i] = k
            np.write()
            sleep_ms(25)
    np.fill([0,0,0])
    np.write()
    sleep_ms(1000)
    # show picture
    for k in range(3):
        for i in range(len(np)):
            np[i] = pic_a[i]
        np.write()
        sleep_ms(1000)
        np.fill([0,0,0])
        np.write()
        sleep_ms(500)

#eof
