# NeoPixel Hardware Test
from machine import Pin
from time import sleep_ms
from neopixel import NeoPixel

NUM_PIX = 8
PIN_PIX = 13

# BIST (Built-In Self Test)
led = Pin(25, Pin.OUT)
for i in range(3):
    led.on()
    sleep_ms(200)
    led.off()
    sleep_ms(200)

# NeopPixel Test
np_pin = Pin(PIN_PIX, Pin.OUT)
np = NeoPixel(np_pin, NUM_PIX)

while True:
    for bright in [2, 16, 255, 8]:
        for colour in range(1, NUM_PIX):
            col_tupel = [((colour >> 2) & 1) * bright,
                         ((colour >> 1) & 1) * bright,
                         (colour & 1) * bright]
            for i in range(NUM_PIX):
                if bright > 31:
                    np.fill([0,0,0])
                np[i] = col_tupel
                np.write()
                sleep_ms(100)
    np.fill([0, 0, 0])
    np.write()
    sleep_ms(1000)
