# NeoPixel Hardware Test
from machine import Pin
from time import sleep_ms
from neopixel import NeoPixel

NUM_PIX = 8
PIN_PIX = 13

# BIST (Built-In Self Test)
led = Pin(25, Pin.OUT)
for i in range(3):
    led.on()
    sleep_ms(200)
    led.off()
    sleep_ms(200)

# NeopPixel Test
np_pin = Pin(PIN_PIX, Pin.OUT)
np = NeoPixel(np_pin, NUM_PIX)
kleuren = [(16,0,0),(0,16,0),(0,0,16),(32,0,0),(0,32,0),(0,0,32)]
while True:
    for k in kleuren:
        for i in range(len(np)):
            np[i] = k
            np.write()
            sleep_ms(200)
    np.fill([0,0,0])
    np.write()
    sleep_ms(1000)


