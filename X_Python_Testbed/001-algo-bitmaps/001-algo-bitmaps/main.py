# PC-Testbed for bitmap manipulation routines.
# Original source:
# https://github.com/rdagger/micropython-ssd1309/blob/master/ssd1309.py
# 2024-10-09 Hagen Patzke

from time import time_ns

OPT_IN_PLACE_CONV = True


def get_byte_size(width):
    return ((width - 1) >> 3) + 1


def prep_bitmap(path, w, h, invert=False, rotate=0):
    """Load MONO_HMSB bitmap from disc and prepare for drawing.

    Args:
        path (string): Image file path.
        x (int): x-coord of image.
        y (int): y-coord of image.
        w (int): Width of image.
        h (int): Height of image.
        invert (bool): True = invert image, False (Default) = normal image.
        rotate(int): 0, 90, 180, 270
    Notes:
        w x h cannot exceed 2048
    """
    w_bytes = get_byte_size(w)
    h_bytes = get_byte_size(h)
    array_size = w_bytes * h
    with open(path, "rb") as f:
        buf = bytearray(f.read(array_size))
        buf_len = len(buf)  # PARANOIA

        if invert:  # if we need to invert, do it with the initial buffer
            if OPT_IN_PLACE_CONV:
                for idx in range(buf_len):
                    buf[idx] ^= 0xFF
            else:
                buf = bytearray(map(lambda b: b ^ 0xFF, buf))

        if rotate == 180:  # 180 degrees -> reverse buffer order and bit order
            buf2 = bytearray(buf_len)
            for idx in range(buf_len):
                # reverse bit order
                cur_byte = buf[idx]
                new_byte = 0
                for i in range(8):
                    new_byte <<= 1
                    new_byte |= (cur_byte & 1)
                    cur_byte >>= 1
                buf2[buf_len - 1 - idx] = new_byte
            buf = buf2
        elif rotate == 90:  # 90 degrees to the right
            buf2_len = h_bytes * w
            buf2 = bytearray(buf2_len)
            for y1 in range(h):
                for x1 in range(w):
                    # fb2.pixel(y1, x1, fb.pixel(x1, (h - 1) - y1))
                    y2 = x1
                    x2 = (h - 1) - y1
                    src_idx = (y1 * w_bytes) + (x1 >> 3)
                    src_bit = x1 & 7
                    dst_idx = (y2 * h_bytes) + (x2 >> 3)
                    dst_bit = x2 & 7
                    val_bit = ((buf[src_idx] >> src_bit) & 1) << dst_bit
                    buf2[dst_idx] |= val_bit
            buf = buf2
        elif rotate == 270:  # 90 degrees to the left
            buf2_len = h_bytes * w
            buf2 = bytearray(buf2_len)
            for y1 in range(h):
                for x1 in range(w):
                    # fb2.pixel(y1, x1, fb.pixel((w - 1) - x1, y1))
                    y2 = (w - 1) - x1
                    x2 = y1
                    src_idx = (y1 * w_bytes) + (x1 >> 3)
                    src_bit = x1 & 7
                    dst_idx = (y2 * h_bytes) + (x2 >> 3)
                    dst_bit = x2 & 7
                    val_bit = ((buf[src_idx] >> src_bit) & 1) << dst_bit
                    buf2[dst_idx] |= val_bit
            buf = buf2

    return buf


def dump_bitmap(w, h, ba):
    w2 = get_byte_size(w)
    i = 0
    for y in range(h):
        s = ''
        for x in range(w2):
            c = ba[i]
            for b in range(8):
                s += '#' if c & 1 else '_'
                c >>= 1
            i = i + 1
        print(s)


### main

print("images/invaders_48x36.mono", 'w:48', 'h:36', 'invert=True', 'rotate=0')
st = time_ns()
bm = prep_bitmap("images/invaders_48x36.mono", 48, 36, invert=True, rotate=0)
dt = time_ns() - st
dump_bitmap(48, 36, bm)
print('Execution time [ns]:', dt, '\n')

print("images/invaders_48x36.mono", 'w:48', 'h:36', 'invert=False', 'rotate=180')
st = time_ns()
bm = prep_bitmap("images/invaders_48x36.mono", 48, 36, invert=False, rotate=180)
dt = time_ns() - st
dump_bitmap(48, 36, bm)
print('Execution time [ns]:', dt, '\n')

print("images/invaders_48x36.mono", 'w:48', 'h:36', 'invert=False', 'rotate=0')
st = time_ns()
bm = prep_bitmap("images/invaders_48x36.mono", 48, 36, invert=False, rotate=0)
dt = time_ns() - st
dump_bitmap(48, 36, bm)
print('Execution time [ns]:', dt, '\n')

print("images/invaders_48x36.mono", 'w:48', 'h:36', 'invert=False', 'rotate=90')
st = time_ns()
bm = prep_bitmap("images/invaders_48x36.mono", 48, 36, invert=False, rotate=90)
dt = time_ns() - st
dump_bitmap(36, 48, bm)
print('Execution time [ns]:', dt, '\n')

print("images/invaders_48x36.mono", 'w:48', 'h:36', 'invert=False', 'rotate=270')
st = time_ns()
bm = prep_bitmap("images/invaders_48x36.mono", 48, 36, invert=False, rotate=270)
dt = time_ns() - st
dump_bitmap(36, 48, bm)
print('Execution time [ns]:', dt, '\n')
