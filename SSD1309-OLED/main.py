# Test for transparent OLED with SSD1309
"""
  YUR-BOT
  128 x 64 pixels, 1.51 inch
  I2C address "0x78"  == 0x3C
  Supply voltage: 5V

  SSD1309 demo (shapes).
  Source: https://github.com/rdagger/micropython-ssd1309
  Port from SPI interface to I2C by Hagen Patzke
"""

from time import sleep_ms
from machine import Pin, I2C
from ssd1309 import Display


def pico_selftest():
    # BIST (Built-In Self Test)
    try:  # RasPi PICO-W has a different LED control way
        led = Pin("LED", Pin.OUT)
    except:
        led = Pin(25, Pin.OUT)
    for i in range(3):
        led.on()
        sleep_ms(200)
        led.off()
        sleep_ms(200)


def test(display):
    display.draw_rectangle(1, 1, 30, 30)
    display.fill_rectangle(6, 6, 20, 20)
    display.present()
    sleep_ms(1000)

    display.fill_circle(50, 16, 14)
    display.draw_circle(50, 16, 10, invert=True)
    display.present()
    sleep_ms(1000)

    coords = [[106, 0], [106, 60], [70, 11], [127, 30], [70, 49], [106, 0]]
    display.draw_lines(coords)
    display.present()
    sleep_ms(1000)

    display.fill_ellipse(16, 48, 15, 8)
    display.draw_ellipse(16, 48, 8, 15)
    display.present()
    sleep_ms(1000)

    display.fill_polygon(5, 50, 48, 8)
    display.draw_polygon(7, 50, 48, 15)
    display.present()
    sleep_ms(1000)

    display.draw_line(117, 63, 127, 53)
    display.draw_vline(127, 53, 10)
    display.draw_hline(117, 63, 10)
    display.present()
    sleep_ms(1000)


### main #########################

# Flash Built-In LED three times
pico_selftest()
# Initialize I2C with pins
i2c = I2C(0, scl=Pin(5), sda=Pin(4), freq=400_000)
# print('I2C scan:', i2c.scan())  # DEBUG

while True:
    display = Display(i2c)
    test(display)
    display.cleanup()  # Turns off display, you need a new init to reset it.

# eof
