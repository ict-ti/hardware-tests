# HelTec 2.13 Inch E-ink Display V2
# https://heltec.org/project/213-e-ink/
# This display has 122 x 250 pixels (128 x 250 bits in RAM)
#
# Hardware Test code
# 2023-03-25 Hagen Patzke - first usable demo
# Copyright Hogeschool Utrecht HBO-ICT TI

# Standard MicroPython imports
from machine import Pin, SPI
from time import sleep_ms
from framebuf import FrameBuffer, MONO_HLSB
# our driver
import heltec2in13v2 as epaper

# SPI0 (default) on Raspberry Pi Pico
d_dc = Pin(14)
d_busy = Pin(15)
d_cs = Pin(17)
d_clk = Pin(18)
d_sdi = Pin(19)

LED = Pin(25, Pin.OUT)  # Pico built-in LED


def led_flash_ms(duration_ms):
    LED.on()
    sleep_ms(duration_ms)
    LED.off()
    sleep_ms(duration_ms)


# Built-In Self Test (Flash LED on Boot six times)
for i in range(6):
    led_flash_ms(300)

# Initialize SPI settings
spi = SPI(0, baudrate=2_000_000, polarity=0, phase=0, bits=8,
          sck=d_clk, mosi=d_sdi, miso=None)

sleep_ms(400)

e = epaper.EPD(spi, d_cs, d_dc, d_busy)
w = epaper.EPD_WIDTH
h = epaper.EPD_HEIGHT

MAX_ROW_TEXT = (h // 8) - 1

# Our frame buffers need ca. 4000 bytes each
numbytes = ((w + 7) // 8) * h
buf_blk = bytearray(numbytes)
buf_red = bytearray(numbytes)
fb_blk = FrameBuffer(buf_blk, w, h, MONO_HLSB)
fb_red = FrameBuffer(buf_red, w, h, MONO_HLSB)
black = 0
white = 1
blk_black = 0
blk_white = 1
red_white = 0
red_red = 1


def epaper_clear():
    e.init()
    fb_blk.fill(blk_white)
    fb_red.fill(red_white)
    e.display_frame(buf_blk, buf_red)
    led_flash_ms(200)


def epaper_test():
    # Test display
    e.init()
    fb_blk.fill(blk_white)
    fb_red.fill(red_white)
    fb_blk.text('Hello World', 30, 0, blk_black)
    fb_blk.text('Hoi       !', 30, 16, blk_black)
    fb_red.text('    n0harm ', 30, 16, red_red)
    fb_red.pixel(30, 10, red_red)
    fb_red.hline(30, 30, 10, red_red)
    fb_blk.vline(30, 50, 10, blk_black)
    fb_red.line(30, 70, 40, 80, red_red)
    fb_blk.rect(30, 90, 10, 10, blk_black)
    fb_red.fill_rect(30, 110, 10, 10, red_red)
    # 212 pixels are 26.5 lines at 8 pixels height
    for row in range(MAX_ROW_TEXT):
        fb_blk.text(str(row), 0, row * 8, blk_black)
    fb_red.text('Line ' + str(MAX_ROW_TEXT), 0, MAX_ROW_TEXT * 8, red_red)
    # red filled circle
    e.draw_filled_circle(buf_red, 80, 60, 30, red_red)
    # can't see any black if the red bit is on
    e.draw_filled_rectangle(buf_red, 70, 50, 90, 70, red_white)
    e.draw_filled_rectangle(buf_blk, 70, 50, 90, 70, blk_black)
    # let's have a white border
    e.draw_rectangle(buf_blk, 70, 50, 90, 70, blk_white)
    # black filled circle
    e.draw_filled_circle(buf_blk, 80, 130, 30, blk_black)
    e.draw_filled_rectangle(buf_blk, 70, 120, 90, 140, blk_white)
    e.draw_rectangle(buf_red, 70, 120, 90, 140, red_red)
    # red circle
    e.draw_circle(buf_red, 80, 200, 30, red_red)
    e.draw_filled_rectangle(buf_blk, 70, 190, 90, 210, blk_black)
    e.display_frame(buf_blk, buf_red)
    led_flash_ms(800)


# main program
epaper_clear()
sleep_ms(20000)
epaper_test()
print(" - Sleep")
e.sleep()  # debug: parameter mode=b'\x00' will not sleep, useful for testing

# eof
