"""
MicroPython HelTec 2.13" QYEG0213RWS800 Black/White/Red e-paper display driver

https://heltec.org/project/213-e-ink/
https://resource.heltec.cn/download/e-ink/213/2.13b%26w/HTEW0213T5_V2/IL0373.pdf
https://resource.heltec.cn/download/e-ink/e-ink-module-user-manual.pdf
https://github.com/HelTecAutomation/e-ink

This driver is for a module board with the inputs
 BUSY, CLK, CS, SDI, D/C, GND, VCC
There is no RST_N input, therefore we can only use Soft Reset.


Inspired by https://github.com/mcauser/micropython-waveshare-epaper

MIT License

Copyright (c) 2023 Heltec
Copyright (c) 2023 Hogeschool Utrecht, Hagen Patzke

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

from micropython import const
from time import sleep_ms

# Display resolution
EPD_WIDTH = const(128)  # 122 visible
EPD_HEIGHT = const(250)

# Display commands -- e_ink.h section "SPECIAL_SCREEN"
DRIVER_OUTPUT_CONTROL = const(0x01)
BOOSTER_SOFT_START_CONTROL = const(0x0C)
GATE_SCAN_START_POSITION = const(0x0F)
DEEP_SLEEP_MODE = const(0x10)
DATA_ENTRY_MODE_SETTING = const(0x11)
SW_RESET = const(0x12)
TEMPERATURE_SENSOR_CONTROL = const(0x1A)
MASTER_ACTIVATION = const(0x20)
DISPLAY_UPDATE_CONTROL_1 = const(0x21)
DISPLAY_UPDATE_CONTROL_2 = const(0x22)
WRITE_RAM_BLACK_WHITE = const(0x24)
WRITE_VCOM_REGISTER = const(0x2C)
WRITE_LUT_REGISTER = const(0x32)
SET_DUMMY_LINE_PERIOD = const(0x3A)
SET_GATE_TIME = const(0x3B)
BORDER_WAVEFORM_CONTROL = const(0x3C)
SET_RAM_X_ADDRESS_START_END_POSITION = const(0x44)
SET_RAM_Y_ADDRESS_START_END_POSITION = const(0x45)
SET_RAM_X_ADDRESS_COUNTER = const(0x4E)
SET_RAM_Y_ADDRESS_COUNTER = const(0x4F)
TERMINATE_FRAME_READ_WRITE = const(0xFF)
# glanced from comments in Arduino code
SELECT_TEMPERATURE_SENSOR = const(0x18)
WRITE_RAM_BLACK0_WHITE1 = const(0x24)
WRITE_RAM_RED1_WHITE0 = const(0x26)
SET_ANALOG_BLOCK_CONTROL = const(0x74)
SET_DIGITAL_BLOCK_CONTROL = const(0x7E)

# Display orientation
ROTATE_0 = const(0)
ROTATE_90 = const(1)
ROTATE_180 = const(2)
ROTATE_270 = const(3)


class EPD:
    def __init__(self, spi, cs, dc, busy):
        self.spi = spi
        self.cs = cs
        self.dc = dc
        self.busy = busy
        self.cs.init(self.cs.OUT, value=1)
        self.dc.init(self.dc.OUT, value=0)
        self.busy.init(self.busy.IN)
        self.width = EPD_WIDTH
        self.height = EPD_HEIGHT
        self.rotate = ROTATE_0

    def _command(self, command, data=None):
        self.dc(0)  # command mode
        self.cs(0)
        self.spi.write(bytearray([command]))
        self.cs(1)
        if data is not None:
            self._data(data)

    def _data(self, data):
        self.dc(1)  # data mode
        self.cs(0)
        self.spi.write(data)
        self.cs(1)

    def init(self):
        self.reset()
        self._command(SET_ANALOG_BLOCK_CONTROL, b'\x54')
        self._command(SET_DIGITAL_BLOCK_CONTROL, b'\x3B')
        self._command(DRIVER_OUTPUT_CONTROL, b'\xF9\x00\x00')  # Display size and driver output control
        self._command(DATA_ENTRY_MODE_SETTING, b'\x03')  # automatic X increment, Y increment
        self._command(SET_RAM_X_ADDRESS_START_END_POSITION, b'\x01\x10')  # 0x0F-->(15+1)*8=128
        self._command(SET_RAM_Y_ADDRESS_START_END_POSITION, b'\x00\x00\xF9\x00')  # 0xF9-->(249+1)=250
        self._command(BORDER_WAVEFORM_CONTROL, b'\x01')
        self._command(SELECT_TEMPERATURE_SENSOR, b'\x80')  # use the internal temperature sensor
        self._command(SET_RAM_X_ADDRESS_COUNTER, b'\x01')  # Set RAM X address count to 0
        self._command(SET_RAM_Y_ADDRESS_COUNTER, b'\x00\x00')  # Set RAM Y address count to 0
        self.wait_until_idle()

    def wait_until_idle(self, timeout_ms=None):
        # 0=idle, 1=busy
        cur_time_ms = 0
        while self.busy.value() == 1:
            sleep_ms(100)
            if timeout_ms is not None:
                cur_time_ms += 100
                if cur_time_ms > timeout_ms:
                    return

    def reset(self):
        # Let's try repeated SW_RESETs.
        # Unfortunately we cannot wake up from deep sleep with SW_RESET.
        self.wait_until_idle(timeout_ms=8000)  # max 8 seconds
        while self.busy.value() == 1:
            self._command(SW_RESET)
            self.wait_until_idle(timeout_ms=8000)

    def sleep(self, mode=b'\x01'):
        """ deep sleep needs HW_RESET to wake up (power cycle) """
        self._command(DEEP_SLEEP_MODE, mode)
        sleep_ms(100)

    def display_update(self):
        """Electrophoretic Display (EPD) Update"""
        self._command(DISPLAY_UPDATE_CONTROL_2, b'\xF7')
        self._command(MASTER_ACTIVATION)
        self.wait_until_idle()

    def display_frame(self, frame_buffer_black, frame_buffer_red):
        if frame_buffer_black is not None:
            self._command(WRITE_RAM_BLACK0_WHITE1)
            for i in range(0, self.width * self.height // 8):
                self._data(bytearray([frame_buffer_black[i]]))
        if frame_buffer_red is not None:
            self._command(WRITE_RAM_RED1_WHITE0)
            for i in range(0, self.width * self.height // 8):
                self._data(bytearray([frame_buffer_red[i]]))
        self.display_update()

    def set_rotate(self, rotate):
        if rotate == ROTATE_0:
            self.rotate = ROTATE_0
            self.width = EPD_WIDTH
            self.height = EPD_HEIGHT
        elif rotate == ROTATE_90:
            self.rotate = ROTATE_90
            self.width = EPD_HEIGHT
            self.height = EPD_WIDTH
        elif rotate == ROTATE_180:
            self.rotate = ROTATE_180
            self.width = EPD_WIDTH
            self.height = EPD_HEIGHT
        elif rotate == ROTATE_270:
            self.rotate = ROTATE_270
            self.width = EPD_HEIGHT
            self.height = EPD_WIDTH

    def set_pixel(self, frame_buffer, x, y, colored):
        if x < 0 or x >= self.width or y < 0 or y >= self.height:
            return
        if self.rotate == ROTATE_0:
            pass
        elif self.rotate == ROTATE_90:
            point_temp = x
            x = EPD_WIDTH - y
            y = point_temp
        elif self.rotate == ROTATE_180:
            x = EPD_WIDTH - x
            y = EPD_HEIGHT - y
        elif self.rotate == ROTATE_270:
            point_temp = x
            x = y
            y = EPD_HEIGHT - point_temp
        else:  # unknown rotate value
            return
        self.set_absolute_pixel(frame_buffer, x, y, colored)

    def set_absolute_pixel(self, frame_buffer, x, y, set_bit):
        """ To avoid display orientation effects
            use EPD_WIDTH instead of self.width
            use EPD_HEIGHT instead of self.height """
        if x < 0 or x >= EPD_WIDTH or y < 0 or y >= EPD_HEIGHT:
            return
        if set_bit:
            frame_buffer[(x + y * EPD_WIDTH) // 8] |= 0x80 >> (x % 8)
        else:
            frame_buffer[(x + y * EPD_WIDTH) // 8] &= ~(0x80 >> (x % 8))

    def draw_string_at(self, frame_buffer, x, y, text, font, colored):
        image = Image.new('1', (self.width, self.height))
        draw = ImageDraw.Draw(image)
        draw.text((x, y), text, font=font, fill=255)
        # Set buffer to value of Python Imaging Library image.
        # Image must be in mode 1.
        pixels = image.load()
        for y in range(self.height):
            for x in range(self.width):
                # Set the bits for the column of pixels at the current position.
                if pixels[x, y] != 0:
                    self.set_pixel(frame_buffer, x, y, colored)

    def draw_line(self, frame_buffer, x0, y0, x1, y1, colored):
        # Bresenham algorithm
        dx = abs(x1 - x0)
        sx = 1 if x0 < x1 else -1
        dy = -abs(y1 - y0)
        sy = 1 if y0 < y1 else -1
        err = dx + dy
        while (x0 != x1) and (y0 != y1):
            self.set_pixel(frame_buffer, x0, y0, colored)
            if 2 * err >= dy:
                err += dy
                x0 += sx
            if 2 * err <= dx:
                err += dx
                y0 += sy

    def draw_horizontal_line(self, frame_buffer, x, y, width, colored):
        for i in range(x, x + width):
            self.set_pixel(frame_buffer, i, y, colored)

    def draw_vertical_line(self, frame_buffer, x, y, height, colored):
        for i in range(y, y + height):
            self.set_pixel(frame_buffer, x, i, colored)

    def draw_rectangle(self, frame_buffer, x0, y0, x1, y1, colored):
        min_x = x0 if x1 > x0 else x1
        max_x = x1 if x1 > x0 else x0
        min_y = y0 if y1 > y0 else y1
        max_y = y1 if y1 > y0 else y0
        self.draw_horizontal_line(frame_buffer, min_x, min_y, max_x - min_x + 1, colored)
        self.draw_horizontal_line(frame_buffer, min_x, max_y, max_x - min_x + 1, colored)
        self.draw_vertical_line(frame_buffer, min_x, min_y, max_y - min_y + 1, colored)
        self.draw_vertical_line(frame_buffer, max_x, min_y, max_y - min_y + 1, colored)

    def draw_filled_rectangle(self, frame_buffer, x0, y0, x1, y1, colored):
        min_x = x0 if x1 > x0 else x1
        max_x = x1 if x1 > x0 else x0
        min_y = y0 if y1 > y0 else y1
        max_y = y1 if y1 > y0 else y0
        for i in range(min_x, max_x + 1):
            self.draw_vertical_line(frame_buffer, i, min_y, max_y - min_y + 1, colored)

    def draw_circle(self, frame_buffer, x, y, radius, colored):
        # Bresenham algorithm
        x_pos = -radius
        y_pos = 0
        err = 2 - 2 * radius
        if x >= self.width or y >= self.height:
            return
        while True:
            self.set_pixel(frame_buffer, x - x_pos, y + y_pos, colored)
            self.set_pixel(frame_buffer, x + x_pos, y + y_pos, colored)
            self.set_pixel(frame_buffer, x + x_pos, y - y_pos, colored)
            self.set_pixel(frame_buffer, x - x_pos, y - y_pos, colored)
            e2 = err
            if e2 <= y_pos:
                y_pos += 1
                err += y_pos * 2 + 1
                if -x_pos == y_pos and e2 <= x_pos:
                    e2 = 0
            if e2 > x_pos:
                x_pos += 1
                err += x_pos * 2 + 1
            if x_pos > 0:
                break

    def draw_filled_circle(self, frame_buffer, x, y, radius, colored):
        # Bresenham algorithm
        x_pos = -radius
        y_pos = 0
        err = 2 - 2 * radius
        if x >= self.width or y >= self.height:
            return
        while True:
            self.set_pixel(frame_buffer, x - x_pos, y + y_pos, colored)
            self.set_pixel(frame_buffer, x + x_pos, y + y_pos, colored)
            self.set_pixel(frame_buffer, x + x_pos, y - y_pos, colored)
            self.set_pixel(frame_buffer, x - x_pos, y - y_pos, colored)
            self.draw_horizontal_line(frame_buffer, x + x_pos, y + y_pos, 2 * (-x_pos) + 1, colored)
            self.draw_horizontal_line(frame_buffer, x + x_pos, y - y_pos, 2 * (-x_pos) + 1, colored)
            e2 = err
            if e2 <= y_pos:
                y_pos += 1
                err += y_pos * 2 + 1
                if -x_pos == y_pos and e2 <= x_pos:
                    e2 = 0
            if e2 > x_pos:
                x_pos += 1
                err += x_pos * 2 + 1
            if x_pos > 0:
                break

# eof
