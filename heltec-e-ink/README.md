## Heltec 2.13 Inch E-ink Display V2

### RPi Pico - Display connections

| #   | Pin  | Function | Display |
|-----|:----:|----------|---------|
| 19  | GP14 | GPIO     | D/C     |
| 20  | GP15 | GPIO     | BUSY    |
| 21  | GP16 | SPI0 RX  | --      |
| 22  | GP17 | SPI0 CSn | CS      |
| 23  | GND  | Ground   | --      |
| 24  | GP18 | SPI0 SCK | CLK     |
| 25  | GP19 | SPI0 TX  | SDI     |
| 36  | 3V3  | 3V3(OUT) | VCC     |
| 38  | GND  | Ground   | GND     |




