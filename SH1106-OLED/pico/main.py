# Test for 128x64 I2C OLED module
from time import sleep_ms
from machine import Pin, I2C
# https://github.com/robert-hh/SH1106
import sh1106

def pico_selftest():
    # BIST (Built-In Self Test)
    try:  # RasPi PICO-W has a different LED control way
        led = Pin("LED", Pin.OUT)
    except:
        led = Pin(25, Pin.OUT)
    for i in range(3):
        led.on()
        sleep_ms(200)
        led.off()
        sleep_ms(200)

## main #########################

# Flash Built-In LED three times
pico_selftest()
# Initialize I2C with pins
i2c = I2C(0, scl=Pin(5), sda=Pin(4), freq=400_000)
#print('I2C scan:', i2c.scan())  # DEBUG
# Display initial message
while True:
    display = sh1106.SH1106_I2C(128, 64, i2c, None, 0x3c)
    display.sleep(False)
    display.fill(0)
    display.text('Testing 1', 0, 0, 1)
    display.show()
    print("OK.")
    sleep_ms(1000)


# next steps
# - check out: https://github.com/carledwards/micropython-gif-viewer
# also interesting:
# - Article: https://forum.micropython.org/viewtopic.php?t=7027
# - Source:  https://github.com/deshipu/circuitpython-gif/tree/master
# also check the original micropython SSD1306:
# - https://docs.micropython.org/en/latest/esp8266/tutorial/ssd1306.html

# eof
