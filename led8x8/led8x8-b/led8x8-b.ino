// DM11A88 LED8x8 module sample
// Simple refresh via ISR and one 8-byte array.
#include <TimerOne.h>

// Hardware connections
const byte pinDatIn = 7;
const byte pinClock = 6;
const byte pinLatch = 5;

// Software configuration
// Example display refresh rates:
// 1s = 1000ms. With 8 lines, we need to allocate 1000ms per 8 lines = 125ms per line.
// We need to refresh 8 lines with 25 Hz -> we need to refresh with 200Hz
//  25Hz ->  1000ms /  (25 Hz * 8 lines) = 5.000 ms / line = 5000 uS / line
//  50Hz ->  1000ms /  (50 Hz * 8 lines) = 2.500 ms / line = 2500 uS / line
//  60Hz ->  1000ms /  (60 Hz * 8 lines) = 2.083 ms / line = 2083 uS / line
// 100Hz ->  1000ms / (100 Hz * 8 lines) = 1.250 ms / line = 1250 uS / line
const int displayRefresh_us = 2083;
const int updateAnimation_ms = 200;

// Display refresh buffer
byte pix[8];
// Animation scheduling and delay
unsigned long t, nextTime = 0L;
byte aniFrame = 0;
// Interrupt Service Routine work variables
byte isrRow = 0;


// Each interrupt refreshes one display row.
// We write row and column data into two chained 74hc595 shift registers.
// _Set_ column bits will light up the respective column(s),
// _Clear_ row bits will light up the respective row(s).
void refreshISR() {
  unsigned int content = (pix[isrRow] << 8) | (0xFF ^ (1 << isrRow));
  digitalWrite(pinLatch, LOW);
  for (byte i = 0; i < 16; i++) {
    digitalWrite(pinClock, LOW);
    digitalWrite(pinDatIn, content & 1);
    digitalWrite(pinClock, HIGH);
    content >>= 1;
  }
  digitalWrite(pinLatch, HIGH);
  digitalWrite(pinClock, LOW); // safety
  // next row, wrap around 0, 1, 2, 3, 4, 5, 6, 7, 8 -> 0, 1, 2, 3, 4, ...
  isrRow = (isrRow + 1) & 7;
}


// Update the animation.
// aniFrame  0..15 -> four repeats of dots 'walking' to the centre
// aniFrame 16..63 -> random numbers into rows
void updateAnimation() {
  if (aniFrame > 15) {
    for (byte r = 0; r < 8; r++)
      pix[r] = random(256);
    // slow refresh, line by line
    // pix[aniFrame & 7] = random(256);
  } else {
    byte pha = aniFrame & 3;
    for (byte r = 0; r < 8; r++) {
      byte row = 0;
      for (byte c = 0; c < 8; c++) {
        if (((c == pha) || (c == (7 - pha)))
            && (((r == pha) || (r == (7 - pha))))) {
          row |= 1 << c;
        }
      } // col
      pix[r] = row;
    } // row
  }

  aniFrame++;
  aniFrame &= 63;
}


void setup() {
  pinMode(pinDatIn, OUTPUT);
  pinMode(pinClock, OUTPUT);
  pinMode(pinLatch, OUTPUT);
  digitalWrite(pinDatIn, LOW);
  // init image
  randomSeed(2874);
  updateAnimation();
  // Set up refresh
  Timer1.initialize(displayRefresh_us);
  Timer1.attachInterrupt(refreshISR);
}


void loop() {
  // update animation after each interval
  t = millis();
  if (t > nextTime) {
    updateAnimation();
    nextTime = t + updateAnimation_ms;
  }
}
