// DM11A88 LED8x8 module sample

const byte pinDatIn = 7;
const byte pinClock = 6;
const byte pinLatch = 5;
const byte maxPwmCount = 3;
const int updateInterval_ms = 200;
unsigned int content;
unsigned long t, nextTime = 0L;
byte pix[64];
byte aniFrame = 0;
byte pwm_count = 0;

void sendColRow(byte cols, byte rows) {
  // columns are active-high, bit=1 -> LED on
  // rows are active=low, bit=0 -> LED column(s) on
  content = (cols << 8) | ((~rows) & 0xFF);
  digitalWrite(pinLatch, LOW);
  for (byte i = 0; i < 16; i++) {
    digitalWrite(pinClock, LOW);
    digitalWrite(pinDatIn, content & 1);
    digitalWrite(pinClock, HIGH);
    content >>= 1;
  }
  digitalWrite(pinLatch, HIGH);
  digitalWrite(pinClock, LOW);
}

void updateAnimation() {
  if (aniFrame > 15) {
    for (byte i = 0; i < 63; i++)
      pix[i] = (random(64) < 10) ? maxPwmCount : 0;
  } else {
    byte pha = aniFrame & 3;
    for (byte r = 0; r < 8; r++)
      for (byte c = 0; c < 8; c++) {
        if (((c == pha) || (c == (7 - pha)))
            && (((r == pha) || (r == (7 - pha))))) {
          pix[r * 8 + c] = maxPwmCount;
        } else {
          pix[r * 8 + c] = 0;
        }
      }
  }

  aniFrame++;
  aniFrame &= 63;
}

void setup() {
  pinMode(pinDatIn, OUTPUT);
  pinMode(pinClock, OUTPUT);
  pinMode(pinLatch, OUTPUT);
  digitalWrite(pinDatIn, LOW);
  // show init image
  sendColRow(9 << 2, 9 << 2);
  delay(500);
  randomSeed(2874);
}


void loop() {
  // update animation after each interval
  t = millis();
  if (t > nextTime) {
    updateAnimation();
    nextTime = t + updateInterval_ms;
  }

  // refresh matrix
  for (byte row = 0; row < 8; row++) {
    for (byte col = 0; col < 8; col++) {
      // brightness of each pixel can be set individually,
      // only one pixel is on at any given time
      if (pwm_count < pix[row * 8 + col])
        sendColRow(1 << col, 1 << row);
    }
  }
  // update PWM counter 0..7
  pwm_count++;
  pwm_count &= maxPwmCount;
}
