// DM11A88 LED8x8 module sample
// Refresh via ISR, one 4 x 8-byte array for simple PWM.
#include <TimerOne.h>

// Hardware connections
const byte pinDatIn = 7;
const byte pinClock = 6;
const byte pinLatch = 5;

// Software configuration
const int displayRefresh_us = 400;
const int updateAnimation_ms = 200;

// Display refresh buffer
byte pix[32];
// Animation scheduling and delay
unsigned long t, nextTime = 0L;
byte aniFrame = 0;
// Interrupt Service Routine work variables
byte isrRow = 0;


// Each interrupt refreshes one display row.
// We write row and column data into two chained 74hc595 shift registers.
// _Set_ column bits will light up the respective column(s),
// _Clear_ row bits will light up the respective row(s).
void refreshISR() {
  unsigned int content = (pix[isrRow] << 8) | (0xFF ^ (1 << (isrRow & 7)));
  digitalWrite(pinLatch, LOW);
  for (byte i = 0; i < 16; i++) {
    digitalWrite(pinClock, LOW);
    digitalWrite(pinDatIn, content & 1);
    digitalWrite(pinClock, HIGH);
    content >>= 1;
  }
  digitalWrite(pinLatch, HIGH);
  digitalWrite(pinClock, LOW); // safety
  // next row, wrap around at end of refresh buffer
  isrRow = (isrRow + 1) & 31; // 8 lines x 4 allows four different brightness values
}


// Update the animation.
// aniFrame  0..15 -> four repeats of dots 'walking' to the centre
// aniFrame 16..63 -> random numbers into rows
void updateAnimation() {
  if (aniFrame > 15) {
    if (0) {
      for (byte r = 0; r < 31; r++)
        pix[r] = random(256);
    } else {
      // slow refresh, line by line
      pix[(aniFrame & 31)] = random(256);
    }
  } else {
    byte pha = aniFrame & 3;
    for (byte r = 0; r < 8; r++) {
      byte row = 0;
      for (byte c = 0; c < 8; c++) {
        if (((c == pha) || (c == (7 - pha)))
            && (((r == pha) || (r == (7 - pha))))) {
          row |= 1 << c;
        }
      } // col
      if (1) {
        pix[r + 0x00] = row;
        pix[r + 0x08] = row & ((r < 4) ? 0xF0 : 0x0F);
        pix[r + 0x10] = row & ((r < 4) ? 0xF0 : 0x0F);
        pix[r + 0x18] = row & ((r < 4) ? 0xF0 : 0x0F);
      } else {
        pix[r + 0x00] = row;
        pix[r + 0x08] = (pha < 3) ? row : 0x00;
        pix[r + 0x10] = (pha < 2) ? row : 0x00;
        pix[r + 0x18] = (pha < 1) ? row : 0x00;
      }

    } // row
  }

  aniFrame++;
  aniFrame &= 63;
}


void setup() {
  pinMode(pinDatIn, OUTPUT);
  pinMode(pinClock, OUTPUT);
  pinMode(pinLatch, OUTPUT);
  digitalWrite(pinDatIn, LOW);
  // init image
  randomSeed(2874);
  updateAnimation();
  // Set up refresh
  Timer1.initialize(displayRefresh_us);
  Timer1.attachInterrupt(refreshISR);
}


void loop() {
  // update animation after each interval
  t = millis();
  if (t > nextTime) {
    updateAnimation();
    nextTime = t + updateAnimation_ms;
  }
}
