# MH-ET LIVE 2.9 Inch E-paper Module
#
# Hardware Test code
# 2023-03-23 Hagen Patzke
# Copyright Hogeschool Utrecht HBO-ICT TI

# Standard MicroPython imports
from machine import Pin, SPI
from time import sleep_ms
from framebuf import FrameBuffer, MONO_HLSB
# our driver
import epaper2in9b as epaper

# SPI0 (default) on Raspberry Pi Pico
d_dc = Pin(13)
d_busy = Pin(15)
d_cs = Pin(17)
d_clk = Pin(18)
d_sdi = Pin(19)
d_res = Pin(14)

LED = Pin(25, Pin.OUT)  # Pico built-in LED


def led_flash_ms(duration_ms):
    LED.on()
    sleep_ms(duration_ms)
    LED.off()
    sleep_ms(duration_ms)


# Built-In Self Test (Flash LED on Boot six times)
for i in range(6):
    led_flash_ms(300)

# Initialize SPI settings
spi = SPI(0, baudrate=2_000_000, polarity=0, phase=0, bits=8,
          sck=d_clk, mosi=d_sdi, miso=None)

sleep_ms(400)

e = epaper.EPD(spi, d_cs, d_dc, d_res, d_busy)
w = epaper.EPD_WIDTH
h = epaper.EPD_HEIGHT

# Our frame buffers needs 104 * 212 / 8 = 2626 bytes each
buf_blk = bytearray((w * h) // 8)
buf_red = bytearray((w * h) // 8)
fb_blk = FrameBuffer(buf_blk, w, h, MONO_HLSB)
fb_red = FrameBuffer(buf_red, w, h, MONO_HLSB)
black = 0
white = 1

MAX_TEXT_ROW = (epaper.EPD_HEIGHT // 8) - 1

def e_ink_clear_display():
    # clear display and wait 5 seconds
    print("Clear")
    print(" - Init")
    e.init()
    print(" - Fill")
    fb_blk.fill(white)
    fb_red.fill(white)
    print(" - Show")
    e.display_frame(buf_blk, buf_red)
    print(" - Sleep")
    e.sleep()
    led_flash_ms(500)

def e_ink_test_display():
    # test display and wait 5 seconds
    print("Test")
    print(" - Init")
    e.init()
    fb_blk.fill(white)
    fb_red.fill(white)
    print(" - Test")
    fb_blk.text('Hello World', 30, 0, black)
    fb_red.pixel(30, 10, black)
    fb_red.hline(30, 30, 10, black)
    fb_blk.vline(30, 50, 10, black)
    fb_red.line(30, 70, 40, 80, black)
    fb_blk.rect(30, 90, 10, 10, black)
    fb_red.fill_rect(30, 110, 10, 10, black)
    for row in range(0, MAX_TEXT_ROW):
        fb_blk.text(str(row), 0, row * 8, black)
    fb_red.text('Line ' + str(MAX_TEXT_ROW), 0, MAX_TEXT_ROW * 8, black)
    # library test
    # NB: last parameter ('colored') is opposite from framebuffer semantics
    # colored: 1 = black/red, 0 = white
    e.draw_circle(buf_blk, 90, 120, 30, 1)
    e.draw_filled_circle(buf_red, 90, 200, 30, 1)
    print(" - Show")
    e.display_frame(buf_blk, buf_red)
    print(" - Sleep")
    e.sleep()
    led_flash_ms(800)


# main program
# e_ink_clear_display()
# sleep_ms(60000)
e_ink_test_display()
sleep_ms(180000)
print("DONE.")
