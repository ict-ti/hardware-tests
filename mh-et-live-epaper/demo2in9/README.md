## MH-ET LIVE 2.9 Inch E-paper Module

## RPi PICO - Display connections

| Display | Pico | Pin  | Function |
|---------|------|:----:|----------|
| BUSY    | GP15 |  20  | GPIO     |
| RESET   | GP14 |  18  | GPIO     |
| D/C     | GP13 |  17  | GPIO     |
| CS      | GP17 |  22  | SPI0 CSn |
| CLK     | GP18 |  24  | SPI0 SCK |
| SDI     | GP19 |  25  | SPI0 TX  |
| GND     | GND  |  38  | Ground   |
| VCC     | 3V3  |  36  | 3V3(OUT) |

NB: Pico module Pin 21 (GP16) is SPI0 RX,
but this module does not have an SDO (serial data output) pin.

Configuration of the module is BS1 = L (4-Line interface)
