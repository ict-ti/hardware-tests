// PlayMp3.ino - modified DFMiniMp3 library example.
// 2024-05-14 Hagen Patzke
//
// This example will play three tracks in order, each one for 30 seconds.
//
// It expects the sd card to contain three mp3 files but doesn't care whats in them.
//
// dfmp3.playMp3FolderTrack(track);
// sd:/mp3/0001.mp3
// sd:/mp3/0002.mp3
// sd:/mp3/0003.mp3
//
// dfmp3.playFolderTrack(folder, track);
// sd:/01/001.mp3
// sd:/01/002.mp3
// sd:/01/003.mp3

#define RX2MP3_PIN 10
#define TX2MP3_PIN 11

#include <DFMiniMp3.h>

class Mp3Notify;  // forward declaration

#ifdef HAVE_SECOND_HW_SERIAL
typedef DFMiniMp3<HardwareSerial, Mp3Notify> DfMp3;
DfMp3 dfmp3(Serial1);
#else  // no HAVE_SECOND_HW_SERIAL
// Some arduino boards only have one hardware serial port.
// Then a software serial port is needed instead.
#include <SoftwareSerial.h>
SoftwareSerial secondarySerial(RX2MP3_PIN, TX2MP3_PIN);  // RX, TX
// Mp3ChipMH2024K16SS chip does not have checksum, seems to be the problem with our module
typedef DFMiniMp3<SoftwareSerial, Mp3Notify, Mp3ChipMH2024K16SS> DfMp3;
DfMp3 dfmp3(secondarySerial);
#endif  // no HAVE_SECOND_HW_SERIAL

class Mp3Notify {
public:
  static void PrintlnSourceAction(DfMp3_PlaySources source, const char* action) {
    if (source & DfMp3_PlaySources_Sd) {
      Serial.print("SD Card, ");
    }
    if (source & DfMp3_PlaySources_Usb) {
      Serial.print("USB Disk, ");
    }
    if (source & DfMp3_PlaySources_Flash) {
      Serial.print("Flash, ");
    }
    Serial.println(action);
  }
  static void OnError([[maybe_unused]] DfMp3& mp3, uint16_t errorCode) {
    static char* errmsg[10] = {
      "Busy", "Sleeping", "Frame not received", "Checksum", "File Index", "Track or Folder not found",
      "Advertisement", "SD Card Read fail", "Flash read fail", "Entered sleep"
    };
    // see DfMp3_Error for code meaning
    Serial.println();
    Serial.print("Com Error ");
    Serial.print(errorCode);
    Serial.print(" - ");
    Serial.println((errorCode < 1 || errorCode > 10) ? "Unknown or library" : errmsg[errorCode - 1]);
  }
  static void OnPlayFinished([[maybe_unused]] DfMp3& mp3, [[maybe_unused]] DfMp3_PlaySources source, uint16_t track) {
    Serial.print("Play finished for #");
    Serial.println(track);
  }
  static void OnPlaySourceOnline([[maybe_unused]] DfMp3& mp3, DfMp3_PlaySources source) {
    PrintlnSourceAction(source, "online");
  }
  static void OnPlaySourceInserted([[maybe_unused]] DfMp3& mp3, DfMp3_PlaySources source) {
    PrintlnSourceAction(source, "inserted");
  }
  static void OnPlaySourceRemoved([[maybe_unused]] DfMp3& mp3, DfMp3_PlaySources source) {
    PrintlnSourceAction(source, "removed");
  }
};


static int track = 0;
static void PlayNextTrack() {
  // start next track
  track += 1;
  // this example will just start back over with 1 after track 3
  if (track > 3) {
    track = 1;
  }
  //dfmp3.playMp3FolderTrack(track);  // sd:/mp3/0001.mp3, sd:/mp3/0002.mp3, sd:/mp3/0003.mp3
  Serial.print("Play track #");
  Serial.println(track);
  pinMode(TX2MP3_PIN, OUTPUT);
  dfmp3.playFolderTrack(1, track);
  pinMode(TX2MP3_PIN, INPUT);
}


void setup() {
  Serial.begin(115200);

  Serial.println("initializing...");

  dfmp3.begin();
  // for boards that support hardware arbitrary pins
  // dfmp3.begin(10, 11); // RX, TX

  // during development, it's a good practice to put the module
  // into a known state by calling reset().
  // You may hear popping when starting and you can remove this
  // call to reset() once your project is finalized
  dfmp3.reset();

  uint16_t version = dfmp3.getSoftwareVersion();
  Serial.print("version ");
  Serial.println(version);

  uint16_t volume = dfmp3.getVolume();
  Serial.print("volume ");
  Serial.println(volume);
  dfmp3.setVolume(24);

  uint16_t count = dfmp3.getTotalTrackCount(DfMp3_PlaySource_Sd);
  Serial.print("files ");
  Serial.println(count);

  Serial.println("starting...");

  // start the first track playing
  //dfmp3.playMp3FolderTrack(1);  // sd:/mp3/0001.mp3

  //pinMode(TX2MP3_PIN, OUTPUT);
  //dfmp3.playFolderTrack(1, 1);  // sd:/01/001_track_name.mp3
  //pinMode(TX2MP3_PIN, INPUT);   // disable output to prevent disruptions

  PlayNextTrack();
}

void waitMilliseconds(uint16_t msWait) {
  uint32_t start = millis();

  while ((millis() - start) < msWait) {
    // if you have loops with delays, its important to
    // call dfmp3.loop() periodically so it allows for notifications
    // to be handled without interrupts
    dfmp3.loop();
    delay(10);  // 1 ms is excessively fast at 9600bps
  }
}

#define MAX_SEC_PLAY 30
static int globalSecondsWait = MAX_SEC_PLAY;
void loop() {
  waitMilliseconds(1000);
  if (--globalSecondsWait < 1) {
    globalSecondsWait = MAX_SEC_PLAY;
    pinMode(TX2MP3_PIN, OUTPUT);
    Serial.print("Stop track #");
    Serial.println(dfmp3.getCurrentTrack());
    dfmp3.stop();
    pinMode(TX2MP3_PIN, INPUT);  // disable output to prevent disruptions
    // stop() does not trigger an OnPlayFinished() event, so we have to start play from here
    PlayNextTrack();
  }
}
