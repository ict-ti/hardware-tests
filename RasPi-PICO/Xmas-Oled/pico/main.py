"""
  YUR-BOT
  128 x 64 pixels, 1.51 inch
  I2C address "0x78"  == 0x3C
  Supply voltage: 5V

  SSD1309 demo (shapes).
  Source: https://github.com/rdagger/micropython-ssd1309
  Port from SPI interface to I2C by Hagen Patzke
"""

from time import sleep_ms
from machine import Pin, I2C
from ssd1309 import Display


def pico_selftest():
    # BIST (Built-In Self Test)
    try:  # RasPi PICO-W has a different LED control way
        led = Pin("LED", Pin.OUT)
    except:
        led = Pin(25, Pin.OUT)
    for i in range(3):
        led.on()
        sleep_ms(200)
        led.off()
        sleep_ms(200)


def test_shapes(display):
    display.clear_buffers()
    display.draw_rectangle(1, 1, 30, 30)
    display.fill_rectangle(6, 6, 20, 20)
    display.present()
    sleep_ms(1000)

    display.fill_circle(50, 16, 14)
    display.draw_circle(50, 16, 10, invert=True)
    display.present()
    sleep_ms(1000)

    coords = [[106, 0], [106, 60], [70, 11], [127, 30], [70, 49], [106, 0]]
    display.draw_lines(coords)
    display.present()
    sleep_ms(1000)

    display.fill_ellipse(16, 48, 15, 8)
    display.draw_ellipse(16, 48, 8, 15)
    display.present()
    sleep_ms(1000)

    display.fill_polygon(5, 50, 48, 8)
    display.draw_polygon(7, 50, 48, 15)
    display.present()
    sleep_ms(1000)

    display.draw_line(117, 63, 127, 53)
    display.draw_vline(127, 53, 10)
    display.draw_hline(117, 63, 10)
    display.present()
    sleep_ms(1000)


def test_images(display):
    display.clear_buffers()
    display.draw_bitmap("images/eyes_128x42.mono", 0, (display.height // 2) - 21, 128, 42)
    display.present()
    sleep_ms(2000)

    display.clear_buffers()
    display.draw_bitmap("images/doggy_jet128x64.mono", 0, 0, 128, 64, invert=True)
    display.present()
    sleep_ms(2000)

    display.clear_buffers()
    display.draw_bitmap("images/invaders_48x36.mono",  0, 14, 48, 36, rotate=90 )
    display.draw_bitmap("images/invaders_48x36.mono", 40, 14, 48, 36, rotate=0  )
    display.draw_bitmap("images/invaders_48x36.mono", 92, 14, 48, 36, rotate=270)
    display.present()
    sleep_ms(2000)

def test_text(display):
    display.clear_buffers()
    display.draw_text8x8(0,  4, 'Merry Christmas!')
    display.draw_text8x8(0, 16, '  OLED SSD1306  ')
    display.draw_text8x8(0, 24, '  DEMONSTRATION ')
    display.draw_text8x8(0, 36, ' Presented  by  ')
    display.draw_text8x8(0, 44, '  Hogeschool    ')
    display.draw_text8x8(0, 52, ' U t r e c h t  ')
    display.present()
    sleep_ms(5000)


# main #########################

# Flash Built-In LED three times
pico_selftest()
# Initialize I2C with pins
i2c = I2C(0, scl=Pin(5), sda=Pin(4), freq=400_000)
# print('I2C scan:', i2c.scan())  # DEBUG

while True:
    display = Display(i2c)
    test_text(display)
    test_shapes(display)
    test_images(display)
    display.cleanup()  # Turns off display, you need a new init to reset it.

# eof
