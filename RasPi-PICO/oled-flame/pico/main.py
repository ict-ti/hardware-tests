# OLED fire animation
from random import random
from time import sleep_ms, time
from machine import I2C, Pin
from ssd1309 import Display

# I2C pins (preferably hardware pins)
I2C_SCL = Pin(5)
I2C_SDA = Pin(4)
# I2C standard speeds, ready for selecting one by index.
# (5MHz never worked when testing, so I left it out.)
I2C_FRQ = (3_400_000, 1_000_000, 400_000, 100_000)[2]
# Test: Garbage Collection call before each frame.
GC_COLLECT = 0
# Number of animation loops to run for time measurement.
REPETITIONS = 20
# Number of milliseconds to sleep after each frame.
FRAME_SLEEP = 100


def pico_selftest():
    # BIST (Built-In Self Test)
    try:  # RasPi PICO-W has a different LED control way
        led = Pin("LED", Pin.OUT)
    except:
        led = Pin(25, Pin.OUT)
    for i in range(3):
        led.on()
        sleep_ms(200)
        led.off()
        sleep_ms(200)


def test_fire(oled):
    """Attempt at generating a fire animation programmatically.
       (Alas, it does not look very convincing. It is also very slow.)"""
    WIDTH = 128
    HEIGHT = 64
    BLUE_AREA = 16
    for repeat in range(25):
        oled.clear_buffers()
        for y in range(HEIGHT):
            for x in range(WIDTH):
                if y < BLUE_AREA:
                    # Create a randomized spark effect in yellow part
                    if random() > 0.95:
                        oled.draw_pixel(x, y)
                else:
                    # Density decreases as we move up
                    intensity_threshold = 0.2 + 0.5 * (64 - y) / 48
                    if random() > intensity_threshold:
                        oled.draw_pixel(x, y)
        oled.present()


def test_text(display):
    display.clear_buffers()
    display.draw_text8x8(0, 4, 'Merry Christmas!')
    display.draw_text8x8(0, 16, '  OLED SSD1306  ')
    display.draw_text8x8(0, 24, '  DEMONSTRATION ')
    display.draw_text8x8(0, 36, ' Presented  by  ')
    display.draw_text8x8(0, 44, '  Hogeschool    ')
    display.draw_text8x8(0, 52, ' U t r e c h t  ')
    display.present()


def test_images_f1(display):
    """Test image animation by pre-loading sprites (frame buffers) and playing them rapidly."""
    fbuf = [
        display.load_sprite(f"px128w64h/f00{x}.mono", 128, 64) for x in range(1, 5)
    ]
    # display.clear_buffers()  -- full screen, we don't need to clear before
    time_draw = 0
    time_show = 0
    rpt = 0
    if GC_COLLECT:
        import gc
    wall_t0 = time()
    for repeat in range(REPETITIONS):
        for i in (0, 1, 2, 3, 2, 1):
            if GC_COLLECT:
                gc.collect()
            t0 = time()
            display.draw_sprite(fbuf[i], 0, 0, 128, 64)
            t1 = time()
            display.present()
            t2 = time()
            time_draw += (t1 - t0)
            time_show += (t2 - t1)
            rpt += 1
            sleep_ms(FRAME_SLEEP)
    wall_t1 = time()
    wall_t = wall_t1 - wall_t0
    # show statistics
    # display.clear_buffers()
    # display.draw_text8x8(0, 4, f'Statistics (gc={GC_COLLECT})')
    # display.draw_text8x8(0, 16, f'draw:{time_draw * 1000 / rpt:.3f}ms')
    # display.draw_text8x8(0, 26, f'show:{time_show * 1000 / rpt:.3f}ms')
    # display.draw_text8x8(0, 36, f'freq={I2C_FRQ}')
    # display.draw_text8x8(0, 46, f'wall:{wall_t * 1000 / rpt:.3f}ms')
    # display.present()
    print(f'f1,gc={GC_COLLECT},rept:{rpt},freq={I2C_FRQ},'
          + f'draw:{time_draw * 1000 / rpt:.3f}ms,'
          + f'show:{time_show * 1000 / rpt:.3f}ms,'
          + f'wall:{wall_t * 1000 / rpt:.3f}ms,'
          + f'sleep:{FRAME_SLEEP}ms')


def test_images_g1(display):
    """Better animation with more phases, by duplicating each frame with
       its mirror image."""
    fbuf = []
    maxi = 0
    for x in range(4):
        buf = display.load_buf(f"px128w64h/g{x}.mono", 128, 64)
        fbuf.append(display.make_fb(buf, 128, 64))
        maxi += 1
        fbuf.append(display.make_fb(display.mirror_horizontal(buf, 128, 64), 128, 64))
        maxi += 1
    displaylist = list(range(0, maxi)) + list(range(maxi - 2, 0, -1))
    # print("show", maxi, "list", displaylist)
    # display.clear_buffers()  -- full screen, we don't need to clear before
    time_draw = 0
    time_show = 0
    rpt = 0
    if GC_COLLECT:
        import gc
    wall_t0 = time()
    for repeat in range(REPETITIONS):
        for i in displaylist:
            if GC_COLLECT:
                gc.collect()
            t0 = time()
            display.draw_sprite(fbuf[i], 0, 0, 128, 64)
            t1 = time()
            display.present()
            t2 = time()
            time_draw += (t1 - t0)
            time_show += (t2 - t1)
            rpt += 1
            sleep_ms(FRAME_SLEEP)
    wall_t1 = time()
    wall_t = wall_t1 - wall_t0
    # show statistics
    print(f'g1,gc={GC_COLLECT},rept:{rpt},freq={I2C_FRQ},'
          + f'draw:{time_draw * 1000 / rpt:.3f}ms,'
          + f'show:{time_show * 1000 / rpt:.3f}ms,'
          + f'wall:{wall_t * 1000 / rpt:.3f}ms,'
          + f'sleep:{FRAME_SLEEP}ms')


def test_images_g2(display):
    """Eliminating draw_sprite (blit) by formatting the input buffer
       from horizontal layout (natural file format)
       to vertical format (which the OLED needs) during pre-load.
       This takes quite some time (the blit() call we save is a native function),
       but it's only done once at startup when pre-loading the data."""
    fbuf = []
    maxi = 0
    for x in range(4):
        buf = display.load_buf(f"px128w64h/g{x}.mono", 128, 64)
        fbuf.append(display.buf_hmsb_to_vlsb(buf, 128, 64))
        maxi += 1
        fbuf.append(display.buf_hmsb_to_vlsb(display.mirror_horizontal(buf, 128, 64), 128, 64))
        maxi += 1
    displaylist = list(range(0, maxi)) + list(range(maxi - 2, 0, -1))
    # print("show", maxi, "list", displaylist)
    # display.clear_buffers()  -- full screen, we don't need to clear before
    time_show = 0
    rpt = 0
    if GC_COLLECT:
        import gc
    wall_t0 = time()
    for repeat in range(REPETITIONS):
        for i in displaylist:
            if GC_COLLECT:
                gc.collect()
            t0 = time()
            display.present_buf(fbuf[i], 128, 64)
            t1 = time()
            time_show += (t1 - t0)
            rpt += 1
            sleep_ms(FRAME_SLEEP)
    wall_t1 = time()
    wall_t = wall_t1 - wall_t0
    # show statistics
    print(f'g2,gc={GC_COLLECT},rept:{rpt},freq={I2C_FRQ},'
          + f'draw:0ms,'
          + f'show:{time_show * 1000 / rpt:.3f}ms,'
          + f'wall:{wall_t * 1000 / rpt:.3f}ms,'
          + f'sleep:{FRAME_SLEEP}ms')

# main #########################

# Flash Built-In LED three times
pico_selftest()
# Initialize I2C with pins and frequency
i2c = I2C(0, scl=I2C_SCL, sda=I2C_SDA, freq=I2C_FRQ)
# print('I2C scan:', i2c.scan())  # DEBUG
display = Display(i2c)

while True:
    test_text(display)
    sleep_ms(1_000)  # without a delay here we can't read any of it :-)
    # test_fire(display) # awfully slow, so we disable it here
    test_images_f1(display)
    test_images_g1(display)
    test_images_g2(display)
