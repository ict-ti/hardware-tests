// Test program for Arduino Due ports

const int interval = 50; // blink for this time before switching to the next port

int ledPin;

void setup() {
  Serial.begin(115200);
}

void loop() {
  Serial.println("Digital pins");
  // Start from 2 so we don't overrride RX/TX function for serial console!
  for ( ledPin = 2; ledPin <= 53; ledPin++ ) { 
    Serial.print((ledPin < 10)?"  ":" ");
    Serial.print(ledPin);
    if (0x0F == (ledPin & 0x0F))
      Serial.println();
    pinMode(ledPin, OUTPUT);
    digitalWrite(ledPin, HIGH);
    delay(interval);
    digitalWrite(ledPin, LOW);
    delay(interval);
  }
  Serial.println();
}
